:- dynamic ([
    % Map - could be static, but we can init different maps this way
    size/1,
    wumpusPos/1,
    pitPos/1,
    goldPos/1,
    
    % Knowledge base
    isPit/2,
    isWumpus/2,
    isGold/2,
    isWumpusDead/1
    ]).

% go(+N) :- MAIN
% entry point of the app
% N is number of the map we want to play
go(N) :-
    writeln('Initialization'), 
    initialize(N),
    writeln('Simulation started'),
    simulate([1,1],[[1,1]], 1, 1),!.

%--------------------%
%%% Initialization %%%
%--------------------%
% initialize(+N) :-
% N is setup we want to initialize
initialize(N) :-
        initKB,
        initSize(N),
        initWumpus(N),
        initGold(N),
        initPits(N).
    
initKB :-
        retractall(isPit(_, _)),
        retractall(isGold(_, _)),
        retractall(isWumpus(_, _)),
        retractall(isWumpusDead(_)),
        assert(isPit([1,1], false)),
        assert(isWumpus([1,1], false)).

initSize(0) :-
        retractall(size(_)),
        assert(size([4,4])).
initSize(1) :- initSize(0).
initSize(2) :-
	retractall(size(_)),
	assert(size([5,5])).

initWumpus(0) :-
        retractall(wumpusPos(_)),
        assert(wumpusPos([1,3])).
initWumpus(1) :-
    retractall(wumpusPos(_)), 
    assert(wumpusPos([2,3])).
initWumpus(2) :- 
    retractall(wumpusPos(_)),
    assert(wumpusPos([2,5])).        
        
initGold(0) :-
        retractall(goldPos(_)),
        assert(goldPos([2,3])).
initGold(1) :-
    retractall(goldPos(_)),
    assert(goldPos([2,2])).
initGold(2) :-
	retractall(goldPos(_)),
	assert(goldPos([2,4])).

initPits(0) :-
        retractall(pitPos(_)),
        assert(pitPos([3,1])),
        assert(pitPos([3,3])),
        assert(pitPos([4,4])).
initPits(1) :-
        retractall(pitPos(_)),
        assert(pitPos([3,1])),
        assert(pitPos([1,3])).
initPits(2) :-
	retractall(pitPos(_)),
	assert(pitPos([1,3])),
	assert(pitPos([3,4])),
	assert(pitPos([5,1])).
        
%----------------%
%%% Simulation %%%
%----------------%
% simulate(+Agent, +Visited, +Spear, +Step) :-
% recursive function simulating agent behaviour and game responses
simulate(Agent, Visited, Spear, Step) :-
        format("----------------- Step ~p~n", [Step]),
        format("-- Agent at ~p~n", [Agent]),
        
        % always check what we see and update the KB
        getPrecepts(Agent, Precepts),
        tellKB(Agent, Precepts),
        
        % either win the game, or try to take a safe move
        (
		    canFinish(Agent)
		    ;
		    tryToKillWumpus(Spear, NewSpear), % always try to kill -> it may open new possibilities
		    
		    % find all possible actions and choose the closet one
		    findall(Pos, askKB(Visited, Pos), Goals),
		    checkNotEmpty(Goals),
		    append(Visited, Goals, Allowed),
		    
		    writeln('Finding closest safe spot'),
		    closestStep(Allowed, [Agent], [Agent], Goals, NewAgent),
		    shortestPath([NewAgent|Visited], Agent, NewAgent, Path), 
		    
		    !, % you can not take your action back (if you die for example)
		    format("Agent goes to ~p~n", [NewAgent]),
		    write('Agent uses path: '),
		    writePath(Path),
		    
		    % if we are still alive -> iterate
		    isAlive(NewAgent),
		    NextStep is Step + 1,
		    simulate(NewAgent, [NewAgent|Visited], NewSpear, NextStep)
        ).

% alive dead is done this way so we can easily write where agent died
% isAlive(+Agent) :- check whether agent still lives
isAlive(Agent) :-
        not(isDead(Agent)).
% isDead(+Agent) :- check whether is agent dead
isDead(Agent) :-
        (wumpusPos(Agent), format("The agent died at ~p. Killed by Wumpus.", [Agent]))
        ;
        (pitPos(Agent), format("The agent died at ~p. Fell into a pit.", [Agent])).

% canFinish(+Agent) :- if succeeds game is over -> agent has gold
canFinish(Agent) :-
        isGold(Agent, true),
        goldPos(Agent),
        format("Agent found the gold at ~p. Agent wins!", [Agent]).

% writePath(+Path).
writePath([]) :- writeln('').
writePath([H|T]) :-
	format("~p ", [H]),
	writePath(T).

% checkNotEmpty(+Positions) :- 
% if list is empty -> there is no place to go -> game over
checkNotEmpty([]) :-
        writeln('No place to go GAME OVER'), fail.
checkNotEmpty([_|_]).

% shortestPath(+Allowed, +A, +B, -Path) :-
% finds shortest path from A to B using only Allowed tiles
shortestPath(Allowed, A, B, Path) :-
	findall(P, path(Allowed, [A], A, B, [A], P), Paths),
	((Paths = [], writeln('Could not find path'))
	;
	findShortest(Paths, [], Path)).

% findShortest(+Paths, +CurrentShortest, -Shortest) :-
% from given list of Paths finds the shortest one
% CurrentShortest should be initialized to [] 
findShortest([], Path, Path).
findShortest([H|T], [], Path) :-
    findShortest(T, H, Path),!.
findShortest([H|T], Current, Path) :-
	length(H, Lh),
	length(Current, Lc),
	((Lh < Lc, findShortest(T, H, Path))
	;
	Lh >= Lc, findShortest(T, Current, Path)).

% path(+Allowed, +Visited, +A, +B, +Acc, -Path) :-
% finds a path from A to B using only Allowed tiles
% search done by DFS
path(_, _, A, A, Acc, Path) :-
	reverse(Acc, Path).
path(Allowed, Visited, A, B, Acc, Path) :-
	getNear(A, Near),
	member(Near, Allowed),
	not(member(Near, Visited)),
	path(Allowed, [Near|Visited], Near, B, [Near|Acc], Path).

% closestStep(+Allowed, +Visited, +Frontier, +Goals, -Step) :-
% finds the closest goal from Goals to Agent (init to Frontier)
% uses Wave algorithm
closestStep(_, Visited, _, Goals, Best) :-
	findCommon(Visited, Goals, Best), !.
closestStep(Allowed, Visited, Frontier, Goals, Best) :-
	doStep(Allowed, Visited, Frontier, [], Steps),
	append(Visited, Steps, NewVisited),
	closestStep(Allowed, NewVisited, Steps, Goals, Best).

% doStep(+Allowed, +Visited, +Locations, +Acc, -Steps) :-
% does one step from each Location, removes duplicates
doStep(Allowed, _, [], Acc, Steps) :-
	filter(Acc, Allowed, Steps).
doStep(Allowed, Visited, [H|T], Acc, Steps) :-
        findall(Pos, getNear(H, Pos), NewPos),
        filterReverse(NewPos, Visited, NewPos1), 
        append(NewPos1, Acc, NewAcc),
        append(NewPos1, Visited, NewVisited),
        %(setof(X, member(X, NewAcc), NewAccUniq); NewAccUniq = []),!, % not needed due to visited
        doStep(Allowed, NewVisited, T, NewAcc, Steps).

filterReverse(List, Visited, Filtered) :-
        findall(Item, findNonCommon(List, Visited, Item), Filtered).
filter(List, Allowed, Filtered) :-
        findall(Item, findCommon(List, Allowed, Item), Filtered).

findNonCommon([], _, _) :- fail.
findNonCommon([Item|T], List, Item) :-
        member(Item, List), 
        findNonCommon(T, List, Item).
findNonCommon([Item|_], List, Item) :-
        not(member(Item, List)).
findNonCommon([_|T], List, Item) :-
        findNonCommon(T, List, Item).
% findCommon(+L1, +L2, -Item) :-
% Item is present in both L1 and L2 
findCommon([], _, _) :- fail.
findCommon([Item|_], List, Item) :-
        member(Item, List).
findCommon([_|T], List, Item) :-
        findCommon(T, List, Item).

%
% Wumpus killing %
%
% tryToKillWumpus(+Spear, -NewSpear) :-
% Spear is number of spears we have, NewSpear Spear - number of spear we threw away
% if wumpus is not dead and we have at least one spear we try to kill it
tryToKillWumpus(_,_) :-
        isWumpusDead(true), writeln('Wumpus is dead').
tryToKillWumpus(0, 0) :- writeln('0 spears, cannot kill Wumpus').
tryToKillWumpus(Spear, NewSpear) :-
        Spear > 0,
        findall(Pos, isWumpus(Pos, true), Positions),
        killWumpus(Positions, SpearDiff),
        NewSpear is Spear + SpearDiff.

% killWumpus(+Positions, -SpearDiff) :-
% if we think we know for sure where Wumpus is, we try to kill it
% SpearDiff counts how many spears we should subtract form what we have
killWumpus([], 0) :- writeln('No idea where Wumpus is').
killWumpus([Pos], -1) :-
        (wumpusPos(Pos),
        retractall(wumpusPos(_)),
        retractall(isWumpus(Pos, true)),
        retractall(isPit(Pos, true)),
        assert(isPit(Pos, false)),
        assert(isWumpus(Pos, false)),
        assert(isWumpusDead(true)),
        format("xxx Agent killed Wumpus at ~p~n", [Pos]))
        ;
        format("Agent missed Wumpus at ~p~n", [Pos]).
killWumpus([_|_], 0) :- writeln('Cannot localize Wumpus yet').

%--------------------%
%%% Knowledge base %%%
%--------------------%
%
% TELL %
%
% tellKB(+Agent, +Preceptions) :- 
% updates the database based on agent position and current preceptions
tellKB(Agent, [Breeze, Stench, Glitter]) :-
        writeln('Updading KB'),
        findall(Pos, getNear(Agent, Pos), Positions),
        tellPitAt(Positions, Breeze),
        tellWumpusAt(Positions, Stench),
        tellGoldAt(Agent, Glitter),!.

% tellPitAt(+ListOfPositions, +IsThereAPit?) :-
% for each position updates the database if the new knowledge is beneficial
% if we know somewhare something isn't, it is for sure
% if we think somewhere something is, we just assume
tellPitAt([], _).
tellPitAt([Pos|Xs], Pit) :-
    (isPit(Pos, false);
    isPit(Pos, Pit);
    retractall(isPit(Pos, _)),
    assert(isPit(Pos, Pit)),
    format("   Pit at ~p: ~p~n", [Pos, Pit]))
    ,!,
    tellPitAt(Xs, Pit).

% tellWumpusAt(+ListOfPositions, +IsThereAPit?) :-
% same as tellPitAt, see above
tellWumpusAt([], _).
tellWumpusAt([Pos|Xs], Wump) :-
    (isWumpus(Pos, false);
    isWumpus(Pos, Wump);
    retractall(isWumpus(Pos, _)),
    assert(isWumpus(Pos, Wump)),
    format("   Wumpus at ~p: ~p~n", [Pos, Wump]))
    ,!,
    tellWumpusAt(Xs, Wump).

% tellGoldAt(+Position, +IsThereAGold?) :-
% we can see the gold only if we are on the tile where the gold is
% makes sence only for true values, since if there is a gold we want to grab it and get out
% and the gold knowledge is certain -> we saw the gold, it is there (we do not assume)
tellGoldAt(_, false).
tellGoldAt(Pos, true) :-
        (isGold(Pos, true);
        retractall(isGold(Pos, _)),
        assert(isGold(Pos, true))),
        format("   Gold at ~p: true~n",[Pos]),
        !.

% getNear(+A, -B) :-
% B is near position to A meaning B is either
% above, under, left or right of A
% it also checks B is inside of bounds of map
getNear([X,Y], [U,V]) :-
        size([S1, S2]),
        ((U is X, V is Y + 1);
        (U is X, V is Y - 1);
        (U is X + 1, V is Y);
        (U is X - 1, V is Y)),
        V > 0, V < S1 + 1,
        U > 0, U < S2 + 1.

%
% ASK %
%
% askKB(+Visited, -NewAgent) :-
% NewAgent is position considered safe by the KB for agent to be
askKB(Visited, NewAgent) :- 
    (writeln('Asking KB'),
    isPit(NewAgent, false),
    isWumpus(NewAgent, false),
    not(member(NewAgent, Visited))).
    

%-----------------%
%%% Preceptions %%%
%-----------------%
% getPrecepts(+AgentPos, -Precepts) :-
% checks the map if there are any enviromental effects affecting current agent position
% like Breeze, Stench and Glitter
getPrecepts(AgentPos, [Breeze, Stench, Glitter]) :-
    writeln('Getting precepts'),
    isPitNear(AgentPos, Breeze),
    isWumpusNear(AgentPos, Stench),
    isGoldNear(AgentPos, Glitter),!.

% isNear(+X, +Y) :- true if X is 
% above, under, left or right of Y
isNear([X1, Y1], [X2, Y2]) :-
        Xdiff is X1 - X2,
        Ydiff is Y1 - Y2,
        (Xdiff \= 0 ; Ydiff \= 0), % probably not needed
        ((Xdiff == 0, Ydiff < 2, Ydiff > -2)
        ;(Ydiff == 0, Xdiff < 2, Xdiff > -2)).

% isPitNear(+Agent, -Breeze) :-
% Breeze is precepted only if it is near
isPitNear(Agent, true) :-
        pitPos(Pit),
        isNear(Agent, Pit).
isPitNear(_, false).

% isWumpusNear(+Agent, -Stench)
isWumpusNear(Agent, true) :-
    wumpusPos(Wumpus),
    isNear(Agent, Wumpus).
isWumpusNear(_, false).

% isGoldNear(+Agent, -Glitter) :-
% gold must be on the same spot as agent to be seen
isGoldNear(Agent, true) :-
        goldPos(Agent).
isGoldNear(_, false).