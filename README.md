# Wumpus dungeon logic agent #

### What is this? ###

Prolog implementation of classical problem of Wumpus dungeon presented in Artificial Intelligence a Modern Approach 3rd edition.

I did this as a semester assignment from non procedural programming.

### Documentation ###

The code is explained in comments - each predicate has explanation (English).
There is also documentation in Czech in PDF and docx.

### How to run the agent? ###

1. If you do not have Prolog set up install [SWI-Prolog](http://www.swi-prolog.org/)
2. Run the **wumpusAgent.pl** in Prolog